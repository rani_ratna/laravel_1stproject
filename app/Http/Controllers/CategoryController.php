<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class CategoryController extends Controller
{
    public function createCategory(){
        return view('admin.category.createCategory');
    }

    public function saveCategory(Request $request){
        $this->validate($request,[
        'categoryName'=>'required',
            'categoryDescription'=>'required',
        ]);
     //return $request->all();
        /*Process No. 01*/

       // $category= new Category();
       // $category->categoryName=$request->categoryName;
       // $category->categoryDescription=$request->categoryDescription;
       // $category->publicationStatus=$request->publicationStatus;
       // $category->save();
       // return 'Category info saved successfully!!!!!';

        /*Process No. 02*/

       // Category::create($request->all());
       // return 'Category info saved successfully!!!!!';

        /*Process No. 03*/
        DB::table('categories')->insert([
            'categoryName'=>$request->categoryName,
            'categoryDescription'=>$request->categoryDescription,
            'publicationStatus'=>$request->publicationStatus,
        ]);

       // return redirect()->back();
        return redirect('AddCategory')->with('successMessage','Category Info Saved Successfully');
    }
    public function manageCategory(){
        $categories= Category::all();

        return view('admin.category.manageCategory',['categories'=>$categories]);
    }
    public function editCategory($id){

        $categories= Category::where('id',$id)->first();
       // return $categories;
  //  return view('admin.category.editCategory')->with('categories',$categories);
        return view('admin.category.editCategory',['categories'=>$categories]);
    }

    public function updateCategory(Request $request){
       $category= Category::find($request->id);
       $category->categoryName=$request->categoryName;
       $category->categoryDescription=$request->categoryDescription;
       $category->publicationStatus=$request->publicationStatus;
       $category->save();
       return redirect('ManageCategory')->with('message','Category Info Updated Successfully');

    }
    public function deleteCategory($id){
        $category=Category::find($id);
        $category->delete();
        return redirect('ManageCategory')->with('message','Catrgory Info deleted Successfully');

    }
}
