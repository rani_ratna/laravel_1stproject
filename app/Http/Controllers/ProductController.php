<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Manufacturer;
class ProductController extends Controller
{
    public function createProduct(){
        $categories=Category::where('publicationStatus',1)->get();
        $manufacturers=Manufacturer::where('publicationStatus',1)->get();
        return view('admin.product.createProduct',['categories'=>$categories,'manufacturers'=>$manufacturers]);

    }
}
