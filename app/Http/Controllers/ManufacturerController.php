<?php

namespace App\Http\Controllers;

use App\Category;
use App\Manufacturer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class ManufacturerController extends Controller
{
    public function createManufacturer(){
        return view('admin.manufacturer.createManufacturer');
    }

    public function saveManufacturer(Request $request){
        $this->validate($request,[
        'manufacturerName'=>'required',
            'manufacturerDescription'=>'required',
        ]);
     //return $request->all();
        /*Process No. 01*/

       // $category= new Category();
       // $category->categoryName=$request->categoryName;
       // $category->categoryDescription=$request->categoryDescription;
       // $category->publicationStatus=$request->publicationStatus;
       // $category->save();
       // return 'Category info saved successfully!!!!!';

        /*Process No. 02*/

       // Category::create($request->all());
       // return 'Category info saved successfully!!!!!';

        /*Process No. 03*/
        DB::table('manufacturers')->insert([
            'manufacturerName'=>$request->manufacturerName,
            'manufacturerDescription'=>$request->manufacturerDescription,
            'publicationStatus'=>$request->publicationStatus,
        ]);

       // return redirect()->back();
        return redirect('AddManufacturer')->with('successMessage','Manufacturer Info Saved Successfully');
    }
    public function manageManufacturer(){
        $manufacturer= Manufacturer::all();

        return view('admin.manufacturer.manageManufacturer',['manufacturers'=>$manufacturer]);
    }
    public function editManufacturer($id){

     $manufacture= Manufacturer::where('id',$id)->first();
     return view('admin.manufacturer.editManufacturer',['manufacture'=>$manufacture]);
    }

    public function updateManufacturer(Request $request){
        $manufacture= Manufacturer::find($request->id);
        $manufacture->manufacturerName=$request->manufacturerName;
        $manufacture->manufacturerDescription=$request->manufacturerDescription;
        $manufacture->publicationStatus=$request->publicationStatus;
        $manufacture->save();
       return redirect('ManageManufacturer')->with('message','Manufacturer Info Updated Successfully');

    }
    public function deleteManufacturer($id){
        $manufacture=Manufacturer::find($id);
        $manufacture->delete();
        return redirect('ManageManufacturer')->with('message','Manufacturer Info deleted Successfully');

    }
}
