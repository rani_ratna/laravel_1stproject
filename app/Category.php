<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Integer;

class Category extends Model
{
 protected $fillable=['categoryName','categoryDescription','publicationStatus'];

   public $incrementing=false;
  // protected $casts=['id'=>'Integer'];


}
