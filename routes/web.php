
<?php
Route::get('/','WelcomeController@index');
Route::get('/category','WelcomeController@category');
Route::get('/product-details','WelcomeController@productDetails');

Auth::routes();

Route::get('/dashboard', 'HomeController@index')->name('dashboard');

/*Category Info*/
Route::get('/AddCategory','CategoryController@createCategory');
Route::post('/saveCategory','CategoryController@saveCategory');
Route::get('/ManageCategory','CategoryController@manageCategory');
Route::get('/EditCategory/{id}','CategoryController@editCategory');
Route::post('/UpdateCategory','CategoryController@updateCategory');
Route::get('/DeleteCategory/{id}','CategoryController@deleteCategory');
/*Category Info*/

/*Manufacturer Info*/
Route::get('/AddManufacturer','ManufacturerController@createManufacturer');
Route::post('/saveManufacturer','ManufacturerController@saveManufacturer');
Route::get('/ManageManufacturer','ManufacturerController@manageManufacturer');
Route::get('/EditManufacturer/{id}','ManufacturerController@editManufacturer');
Route::post('/UpdateManufacturer','ManufacturerController@updateManufacturer');
Route::get('/DeleteManufacturer/{id}','ManufacturerController@deleteManufacturer');
/*Manufacturer Info*/

/*Product Info*/
Route::get('/AddProduct','ProductController@createProduct');
Route::post('/SaveProduct','ProductController@saveMProduct');
Route::get('/ManageProduct','ProductController@manageProduct');
Route::get('/EditProduct/{id}','ProductController@editProduct');
Route::post('/UpdateProduct','ProductController@updateProduct');
Route::get('/DeleteProduct/{id}','ProductController@deleteProduct');
/*Product Info*/