@extends('admin.master')

@section('content')


                    <div class="form-group text-center" ><h3>Add Manufacturer</h3></div>
                    <h2 class="text-center text-success">{{Session::get('successMessage')}}</h2>

                    <div class="panel-body">
                        {!! Form::open(['url'=>'/saveManufacturer','method'=>'POST','class'=>'form-horizontal']) !!}
                            <div class="form-group">
                                <label for="categoryName" class="col-sm-12 ">Manufacturer Name</label>
                                <div class="col-sm-10">
                                    <input name="manufacturerName" type="text" class="form-control">
                                    <span>{{$errors->has('manufacturerName')?$errors->first('manufacturerName'):''}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="categoryDescription" class="col-sm-12 ">Category Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="manufacturerDescription" rows="8">
                                    </textarea>
                                    <span class="text-danger">{{$errors->has('manufacturerDescription')?$errors->first('manufacturerDescription'):''}}</span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="publicationStatus" class="col-sm-12 ">Publication Status</label>
                                <div class="col-sm-10">
                                    <select name="publicationStatus" type="text" class="form-control" >
                                        <option>Select Publication Status</option>
                                        <option value="1">Published</option>
                                        <option value="0">Unpublished</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group-lg">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Save Manufacturer Information
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

@endsection