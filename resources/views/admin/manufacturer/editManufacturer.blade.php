@extends('admin.master')

@section('content')


    <div class="form-group text-center" ><h3>Update Manufacturer</h3></div>

    <div class="panel-body">
        {!! Form::open(['url'=>'/UpdateManufacturer','method'=>'POST','class'=>'form-horizontal','name'=>'editManufacturerform']) !!}
        <div class="form-group">
            <label for="categoryName" class="col-sm-12 ">Manufacture Name</label>
            <div class="col-sm-10">
                <input name="manufacturerName" value="{{$manufacture->manufacturerName}}"   type="text" class="form-control" >
                <input name="id" value="{{$manufacture->id}}"   type="hidden" class="form-control" >

            </div>
        </div>

        <div class="form-group">
            <label for="categoryDescription" class="col-sm-12 ">Manufacturer Description</label>
            <div class="col-sm-10">
                                    <textarea class="form-control"  name="manufacturerDescription" rows="8">{{$manufacture->manufacturerDescription}}
                                    </textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="publicationStatus" class="col-sm-12 ">Publication Status</label>
            <div class="col-sm-10">
                <select name="publicationStatus"   type="text" class="form-control" >
                    <option>Select Publication Status</option>
                    <option value="1">Published</option>
                    <option value="0">Unpublished</option>
                </select>
            </div>
        </div>

        <div class="form-group-lg">
            <div class="col-sm-10 col-sm-offset-2">
                <button type="submit" class="btn btn-primary">
                   Update Manufacture Information
                </button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <script>
        document.forms['editManufacturerform'].elements['publicationStatus'].value={{$manufacture->publicationStatus}}
    </script>

@endsection
