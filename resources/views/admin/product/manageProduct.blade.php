@extends('admin.master')

@section('content')
    <h2 class="text-center">Manage Manufacturer</h2>
    <h2 class="text-center text-success">{{Session::get('message')}}</h2>

<table class="table table-bordered">
    <thead>
      <tr>
        <th>Manufacturer ID</th>
        <th>Manufacturer Name</th>
        <th>Manufacturer Description</th>
          <th>Publication Status</th>
          <th>Action</th>
      </tr>
    </thead>
    <tbody>
    @foreach($manufacturers as $manufacture)
      <tr>
        <th scope="row">{{$manufacture->id}}</th>
        <td>{{$manufacture->manufacturerName}}</td>
        <td>{{$manufacture->manufacturerDescription}}</td>
          <td>{{$manufacture->publicationStatus==1?'Published':'Unpublished'}}</td>
          <td>
              <a href="{{url('/EditManufacturer/'.$manufacture->id)}}" class="btn btn-success">
                  <span class=" glyphicon glyphicon-edit"></span>
              </a>
              <a  href="{{url('/DeleteManufacturer/'.$manufacture->id)}}" class="btn btn-danger" onclick="return confirm('Are you sure to delete this data?')">
                  <span class="glyphicon glyphicon-trash"></span>
              </a>
          </td>
      </tr>
    @endforeach
    </tbody>
  </table>

    @endsection
