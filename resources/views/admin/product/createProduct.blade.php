@extends('admin.master')

@section('content')


                    <div class="form-group text-center" ><h3>Add Product</h3></div>
                    <h2 class="text-center text-success">{{Session::get('successMessage')}}</h2>

                    <div class="panel-body">
                        {!! Form::open(['url'=>'/saveManufacturer','method'=>'POST','class'=>'form-horizontal','enctype'=>'multipart/form-data']) !!}
                            <div class="form-group">
                                <label for="productName" class="col-sm-2 control-label ">Product Name</label>
                                <div class="col-sm-10">
                                    <input name="productName" type="text" class="form-control">
                                    <span>{{$errors->has('productName')?$errors->first('productName'):''}}</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="category" class="col-sm-12 "> Category</label>
                                <div class="col-sm-10">
                                    <select name="categoryId" type="text" class="form-control" >
                                        <option>Select Category</option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->categoryName}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="selectManufacturer" class="col-sm-12 "> Manufacturer</label>
                            <div class="col-sm-10">
                                <select name="manufacturerId" type="text" class="form-control" >
                                    <option>Select Manufacturer</option>
                                    @foreach($manufacturers as $manufacturer)
                                        <option value="{{$manufacturer->id}}">{{$manufacturer->manufacturerName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="productPrice" class="col-sm-12 ">Product Price</label>
                            <div class="col-sm-10">
                                <input name="productPrice" type="text" class="form-control">
                                <span>{{$errors->has('productPrice')?$errors->first('productPrice'):''}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="productQuantity" class="col-sm-12 ">Product Quantity</label>
                            <div class="col-sm-10">
                                <input name="productQuantity" type="text" class="form-control">
                                <span>{{$errors->has('productQuantity')?$errors->first('productQuantity'):''}}</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="productShortDescription" class="col-sm-12 ">Product Short Description</label>
                            <div class="col-sm-10">
                                    <textarea class="form-control" name="productShortDescription" rows="8">
                                    </textarea>
                                <span class="text-danger">{{$errors->has('productShortDescription')?$errors->first('productShortDescription'):''}}</span>
                            </div>
                        </div>

                            <div class="form-group">
                                <label for="productDescription" class="col-sm-12 ">Product Description</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control" name="productDescription" rows="8">
                                    </textarea>
                                    <span class="text-danger">{{$errors->has('productDescription')?$errors->first('productDescription'):''}}</span>
                                </div>
                            </div>
                        <div class="form-group">
                            <label for="productImage" class="col-sm-12 ">Product Image</label>
                            <div class="col-sm-10">
                                <input name="productImage" type="file" accept="image/*">
                                <span>{{$errors->has('productPrice')?$errors->first('productPrice'):''}}</span>
                            </div>
                        </div>
                            <div class="form-group">
                                <label for="publicationStatus" class="col-sm-12 ">Publication Status</label>
                                <div class="col-sm-10">
                                    <select name="publicationStatus" type="text" class="form-control" >
                                        <option>Select Publication Status</option>
                                        <option value="1">Published</option>
                                        <option value="0">Unpublished</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group-lg">
                                <div class="col-sm-10 col-sm-offset-2">
                                    <button type="submit" class="btn btn-primary">
                                        Save Product Information
                                    </button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>

@endsection