@extends('admin.master')

@section('content')


    <div class="form-group text-center" ><h3>Update Category</h3></div>

    <div class="panel-body">
        {!! Form::open(['url'=>'/UpdateCategory','method'=>'POST','class'=>'form-horizontal','name'=>'editCategoryform']) !!}
        <div class="form-group">
            <label for="categoryName" class="col-sm-12 ">Category Name</label>
            <div class="col-sm-10">
                <input name="categoryName" value="{{$categories->categoryName}}"   type="text" class="form-control" >
                <input name="id" value="{{$categories->id}}"   type="hidden" class="form-control" >

            </div>
        </div>

        <div class="form-group">
            <label for="categoryDescription" class="col-sm-12 ">Category Description</label>
            <div class="col-sm-10">
                                    <textarea class="form-control"  name="categoryDescription" rows="8">{{$categories->categoryDescription}}
                                    </textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="publicationStatus" class="col-sm-12 ">Publication Status</label>
            <div class="col-sm-10">
                <select name="publicationStatus"   type="text" class="form-control" >
                    <option>Select Publication Status</option>
                    <option value="1">Published</option>
                    <option value="0">Unpublished</option>
                </select>
            </div>
        </div>

        <div class="form-group-lg">
            <div class="col-sm-10 col-sm-offset-2">
                <button type="submit" class="btn btn-primary">
                   Update Category Information
                </button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
    <script>
        document.forms['editCategoryform'].elements['publicationStatus'].value={{$categories->publicationStatus}}
    </script>

@endsection
